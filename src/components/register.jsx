import "./register.css";

function Register() {
  return (
    <>
      <div className="container h-100">
        <div className="d-flex justify-content-center h-100" >
          <div className="user_card">
            <div className="d-flex justify-content-center">
              <h3 id="form-title">REGISTER ACCOUNT</h3>
            </div>
            <div className="d-flex justify-content-center form_container">
              <form method="POST" action="">
                <InputValues icon="fas fa-user" type="text" name="username" />
                <InputValues
                  icon="fas fa-envelope-square"
                  type="email"
                  name="email"
                />
                <InputValues icon="fas fa-key" type="text" name="password1" />
                <InputValues icon="fas fa-key" type="text" name="password2" />

                <div className="d-flex justify-content-center mt-3 login_container">
                  <input
                    className="btn login_btn"
                    type="submit"
                    value="Register Account"
                  />
                </div>
              </form>
            </div>

            <div className="mt-4">
              <div className="d-flex justify-content-center links">
                Already have an account?{" "}
                <a href="{% url 'login' %}" className="ml-2">
                  Login
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  function InputValues(props) {
    return (
      <div className="input-group mb-3">
        <div className="input-group-append">
          <span className="input-group-text">
            <i className={props.icon}></i>
          </span>
        </div>
        <input type={props.type} name={props.name} />
      </div>
    );
  }
}

export default Register;
