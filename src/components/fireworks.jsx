import "./fireworks.css"
function Fireworks() {
  return (
    <>
      <div class="pyro">
        <div class="before"></div>
        <div class="after"></div>
      </div>
    </>
  );
}
export default Fireworks;