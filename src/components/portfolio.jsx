import "./portfolio.css";
import Cars1 from "./images/cars1.jpg";
import Cars2 from "./images/cars2.jpg";
import Cars3 from "./images/cars3.jpg";
import Girl from "./images/girl.jpg";
import Lights from "./images/lights.jpg";
import Nature from "./images/nature.jpg";
import Woman from "./images/woman.jpg";
import Mountain from "./images/mountains.jpg";
import Man from "./images/man.jpg";

function Portfolio() {
  return (
    <>
      <Header />
    </>
  );
}

function ButtonContainer() {
  return (
    <div id="myBtnContainer">
      <button className="btn active">Show all</button>
      <button className="btn">Nature</button>
      <button className="btn">Cars</button>
      <button className="btn">People</button>
    </div>
  );
}
function Header() {
  return (
    <div className="main">
      <h1>MYLOGO.COM</h1>
      <hr />

      <h2>PORTFOLIO</h2>
      <ButtonContainer />
      <Gallery />
    </div>
  );
}
function Gallery() {
  return (
    <>
      <Gallerycolumn img={Mountain} name="Mountains" />
      <Gallerycolumn img={Lights} name="Lights" />
      <Gallerycolumn img={Nature} name="Nature" />
      <Gallerycolumn img={Cars1} name="Retro" />
      <Gallerycolumn img={Cars2} name="Fast" />
      <Gallerycolumn img={Cars3} name="Classic" />
      <Gallerycolumn img={Girl} name="Girl" />
      <Gallerycolumn img={Man} name="Man" />
      <Gallerycolumn img={Woman} name="Woman" />
    </>
  );
}

function Gallerycolumn(props) {
  return (
    <div clasName="row">
      <div className="column ">
        <div className="content">
          <img src={props.img} alt={props.name} style={{ width: "100%" }} />
          <h4>{props.name}</h4>
          <p>Lorem ipsum dolor..</p>
        </div>
      </div>
    </div>
  );
}
export default Portfolio;
